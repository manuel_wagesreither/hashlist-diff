#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
#include <cstdint>
#include <vector>
#include <utility>
#include <fstream>
#include <algorithm>
#include <functional>


typedef std::vector<std::pair<std::string, std::string>> table_t;


enum class Opmode : uint8_t
{
	FIND_CHANGED,
	FIND_DELETED
};


void load_container(const char *filename_arg, table_t &table)
{
	std::ifstream file_stream(filename_arg, std::ios::in);
	for (std::string line; std::getline(file_stream, line); ) {

		// delimiter position is first occurence of space character
		size_t delimiter_position = line.find_first_of(" ");

		// get part from start of line to the delimiter (exclusive)
		std::string hash = line.substr(0, delimiter_position);

		// The remaining line is the filename
		std::string filename = line.substr(delimiter_position + 2, std::string::npos);

		table.emplace_back(
			std::make_pair<std::string, std::string>(
				std::move(hash),
				std::move(filename)
			)
		);
	}
}


void print_stdout(const std::string &hash, const std::string &filename)
{
	std::cout << hash << "  " << filename << std::endl;
}


void print_error()
{
	std::cerr << "Syntax: main OLDFILE NEWFILE" << std::endl;
}


int main(int argc, const char **argv)
{
	if (argc != 4) {
		print_error();
		return -1;
	}

	Opmode opmode;
	if (!std::strcmp(argv[1], "-c")) {
		opmode = Opmode::FIND_CHANGED;
	} else if (!std::strcmp(argv[1], "-d")) {
		opmode = Opmode::FIND_DELETED;
	} else {
		print_error();
		return -1;
	}

	// OLDFILE is specified on second command line parameter
	table_t old_table;
	load_container(argv[2], old_table);

	// NEWFILE is specified on third command line parameter
	table_t new_table;
	load_container(argv[3], new_table);

	auto print_if = [&new_table, &opmode](const std::pair<std::string, std::string>& pair) {
		auto &desired_hash = pair.first;
		auto &desired_filename = pair.second;
		bool found = false;
		std::function<void(const std::pair<std::string, std::string>&)> func;

		auto find_changed = [&desired_hash, &desired_filename, &found](const std::pair<std::string, std::string>& pair) {
			auto &curr_hash = pair.first;
			auto &curr_filename = pair.second;

			if (curr_hash != desired_hash and curr_filename == desired_filename) {
				found = true;
			}
		};
		auto find_hash_or_filename = [&desired_hash, &desired_filename, &found](const std::pair<std::string, std::string>& pair) {
			auto &curr_hash = pair.first;
			auto &curr_filename = pair.second;

			if (curr_hash == desired_hash or curr_filename == desired_filename) {
				found = true;
			}
		};

		switch (opmode) {
		case Opmode::FIND_CHANGED: func = find_changed; break;
		case Opmode::FIND_DELETED: func = find_hash_or_filename; break;
		}

		std::for_each(new_table.begin(), new_table.end(), func);

		switch (opmode) {
		case Opmode::FIND_CHANGED: if (found ==  true) { print_stdout(desired_hash, desired_filename); } break;
		case Opmode::FIND_DELETED: if (found == false) { print_stdout(desired_hash, desired_filename); } break;
		}

	};
	std::for_each(old_table.begin(), old_table.end(), print_if);

	return 0;
}
