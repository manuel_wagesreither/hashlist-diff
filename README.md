# Introduction

When creating a backup snapshot, the last step I do is creating a
`checksums.md5` file of all data backed up. A few times I found myself
in need of a tool to analyse and compare these files. Based on the file
path and the checksum, I would like to classify each file into one of
the following categories:

* New files (= they exist on the newer snapshot only)
* Changed files
* Moved files
* Deleted files

#### New files

A file is categorized as new, if `old.md5` contains no entry with either
the same file path nor the same checksum as found in `new.md5`.

#### Changed files

A file is categorized as changed if `old.md5` and `new.md5` contain
entries holding the same file path with varying checksums.

#### Moved files

A file is categorized as moved if `old.md5` and `new.md5` contain
entries holding the same checksum but a different file path.

#### Deleted files

A file is categorized as deleted if `new.md5` contains no entry with
either the same file path nor the same checksum as found in `old.md5`.

#### Files moved and changed at the same time

It is not possible to detect such files based on the data available.


# Usage

`./hashlist-diff {-n,-c,-m,-d} old.md5 new.md5`
* `-n`	List new files (*not yet implemented*)
* `-c`	List changed files
* `-m`	List moved files (*not yet implemented*)
* `-d`	List deleted files
